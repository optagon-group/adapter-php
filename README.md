# Optagon Group Adapter class for PHP based websites

When a customer creates a Custom Kit and clicks the Add to Basket,
the Kit Calculator will attempt to send data to your website so that it can be added to
the customer's basket on your site. The Adapter Class will decrypt the payload using the
Optagon provided Private Key. You can then use the decrypted kit data to add a product
to the cart. The Adapter will then encrypt a response to send back to the Kit Calculator.
This handshake is all done via an Ajax call from the Kit Calculator and the user
is provided with an error alert should the handshake fail.

## Usage

```php

use CustomSumIT\Adapter\Adapter;

try {
	$Adapter = new Adapter ( $privateKey );

	// KitBuilder Kit Data
	$kit = $Adapter->data;

	// Add to cart START
	$addtoCartSuccess = true;
	// Add to cart END

	// Send a response to the Kit Builder
	$Adapter->setReturnUrl ( $returnUrl )->setAddedToCart ( $addtoCartSuccess )->respond ();
} catch ( Exception $e ) {

	// Log $e->getMessage() for debugging
}
```

## Kit Calculator requirements

When set up, please provide the Kit Calculator team with a URL with which to make the
handshake (http://yourdomain.com/add-to-basket.php)
