<?php
namespace CustomSumit\Adapter;
use \Exception;

/**
 * Connector
 *
 * @author James Herring <james@fluid7.co.uk>
 * @package KitBuilder
 * @version 1.0.1
 */
class Adapter {

	/**
	 * returnUrl
	 * Cart URL
	 */
	private $returnUrl = '';

	/**
	 * addedToCart
	 * Check if the product was added to the merchants cart
	 *
	 */
	private $addedToCart = false;

	/**
	 * data
	 * Cart info
	 */
	public $data = null;

	/**
	 * privateKey
	 * Merchant's Private Key
	 */
	private $privateKey = null;

	/**
	 * publicKey
	 * Skyseal's Public Key
	 */
	private $publicKey = '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs+udf38f0ZZfc6yNfe3c
GBPbstw82lhDD9iXq5+7INv6xWjx3jjyrGXDh+t/XbIxvxIXmGZWEU00fJaJdatq
ddNNY9N9xXo9W3BDslhpIUimlD2bSDtALoCTdJxTF2C63JLfgJwkcK2RvsF0wJq1
3DyzSKBHTFXeKYRFzwI+yrdSObT6j++aphDkVszqWYzM5UV7a7z3MhEoRiMQJyiE
pjmRy/pIQJMQQhD3VWweGW9zqaxnnqgg5Y43f5kELmosDi3waSA3FPswq4Rn7Amb
jJ0CYqPkIdbJhGjQWpTIT7yxHn3rGs2X7mCc4AccrS2lC6DLCyjhjdH7T7HhB75h
fwIDAQAB
-----END PUBLIC KEY-----';

	/**
	 * __construct
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @param String $privateKey
	 * @param String $type key or path to key
	 * @throws Exception
	 * @return void
	 */
	public function __construct($privateKey) {
		header('Content-Type: application/javascript');

		// Is this a private key, or a path?
		if (substr($privateKey, 0, 27) == '-----BEGIN PRIVATE KEY-----') {
			$this->privateKey = $privateKey;
		} else {

			// Set Private Key Path if it exists
			if (!file_exists($privateKey)) {
				throw new Exception("Can't locate private key, check file path");
			}
			$this->privateKey = file_get_contents($privateKey);
		}

		// Decrypt Url Data
		$this->decryptData();
	}

	/**
	 * respond
	 *
	 * @author James Herring
	 * @access public
	 * @return void
	 */
	public function respond() {

		// Set response data
		$encodedData = json_encode(array(
			'url' => $this->returnUrl,
			'success' => $this->addedToCart
		));

		// Encrypt data
		openssl_public_encrypt($encodedData, $encryptedData, $this->publicKey);
		$encodedResponse = json_encode(array(
			'data' => urlencode(base64_encode($encryptedData))
		));

		// Return the JSONP response
		echo $_GET['callback'] . "({$encodedResponse})";
	}

	/**
	 * decryptData
	 *
	 * Decrypts and decodes data
	 *
	 * @author James Herring
	 * @access private
	 * @return void
	 */
	private function decryptData () {

		// Get the private Key
		if (!$privateKey = openssl_pkey_get_private($this->privateKey))
		{
			throw new Exception("Error trying to get Private Key");
		}
		$a_key = openssl_pkey_get_details($privateKey);

		// Decode Encrypted Data
		$encrypted = base64_decode($_GET['data']);
		$output = "";

		// Smaller chunks
		$chunkSize = ceil($a_key['bits'] / 8);

		// Decrypt each chunk
		while ($encrypted) {
			$chunk = substr($encrypted, 0, $chunkSize);
			$encrypted = substr($encrypted, $chunkSize);
			$decrypted = '';

			// Decrypt the chunk using the private key and store the results in $decrypted
			if (!openssl_private_decrypt($chunk, $decrypted, $privateKey)) {
				throw new Exception("Unable to decrypt data, Check private key");
			}
			$output .= $decrypted;
		}

		// Json decode
		if (!$this->data = json_decode($output, true)) {
			throw new Exception("Unable to decode data");
		}
	}

	/**
	 * __call
	 *
	 * Magic Set Method
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @param mixed $name
	 * @param mixed $arguments
	 * @return void
	 */
	public function __call($name, $arguments) {

		// Find a property with the same name
		if (preg_match('/^set.*/',$name) && count($arguments) == 1) {
			$property = lcfirst(substr($name, 3));
			if (property_exists($this, $property)) {

				// Assign value
				$this->$property = $arguments[0];
				return $this;
			}
		}

		// On error
		throw new Exception("That method does not exist");
	}
}
